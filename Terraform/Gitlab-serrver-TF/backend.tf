terraform {
  backend "s3" {
    bucket         = "tf-tetris-backet"
    key            = "ec2/terraform.tfstate"
    region         = "eu-north-1"
    dynamodb_table = "Lock-Files"
    encrypt        = true

  }
}
