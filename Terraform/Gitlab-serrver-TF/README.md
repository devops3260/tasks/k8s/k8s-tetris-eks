# Gitlab-serrver-TF

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 5.42.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.42.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_instance_profile.instance-profile](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.iam-role](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/iam_role) | resource |
| [aws_instance.ec2](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/instance) | resource |
| [aws_internet_gateway.ig](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/internet_gateway) | resource |
| [aws_key_pair.key](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/key_pair) | resource |
| [aws_route_table.rt](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/route_table) | resource |
| [aws_route_table_association.rt-association](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/route_table_association) | resource |
| [aws_security_group.security-group](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/security_group) | resource |
| [aws_subnet.public-subnet](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/subnet) | resource |
| [aws_vpc.vpc](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/resources/vpc) | resource |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/5.42.0/docs/data-sources/ami) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | n/a | `any` | n/a | yes |
| <a name="input_iam-role"></a> [iam-role](#input\_iam-role) | n/a | `any` | n/a | yes |
| <a name="input_igw-name"></a> [igw-name](#input\_igw-name) | n/a | `any` | n/a | yes |
| <a name="input_instance-name"></a> [instance-name](#input\_instance-name) | n/a | `any` | n/a | yes |
| <a name="input_rt-name"></a> [rt-name](#input\_rt-name) | n/a | `any` | n/a | yes |
| <a name="input_sg-name"></a> [sg-name](#input\_sg-name) | n/a | `any` | n/a | yes |
| <a name="input_subnet-name"></a> [subnet-name](#input\_subnet-name) | n/a | `any` | n/a | yes |
| <a name="input_vpc-name"></a> [vpc-name](#input\_vpc-name) | n/a | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ec2_public_ip"></a> [ec2\_public\_ip](#output\_ec2\_public\_ip) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
