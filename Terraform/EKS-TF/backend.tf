terraform {
  backend "s3" {
    bucket         = "tf-tetris-backet"
    key            = "eks/terraform.tfstate"
    region         = "eu-north-1"
    dynamodb_table = "Lock-Files"
    encrypt        = true

  }
}
