terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.46"
    }

  }
  required_version = "~> 1.3"
}

provider "aws" {
  profile                  = "default"
  shared_credentials_files = ["~/.aws/credentials"]
  region                   = var.aws_region

}
