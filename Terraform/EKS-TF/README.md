# Terraform

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.46 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.46 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_eks_cluster.eks-cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster) | resource |
| [aws_eks_node_group.eks-node-group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group) | resource |
| [aws_iam_role.EKSClusterRole](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.NodeGroupRole](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.AmazonEKSClusterPolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_route_table.rt2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.rt-association2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_subnet.public-subnet2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_internet_gateway.igw](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/internet_gateway) | data source |
| [aws_security_group.sg-default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/security_group) | data source |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_vpc.vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | n/a | `any` | n/a | yes |
| <a name="input_cluster-name"></a> [cluster-name](#input\_cluster-name) | n/a | `any` | n/a | yes |
| <a name="input_eksnode-group-name"></a> [eksnode-group-name](#input\_eksnode-group-name) | n/a | `any` | n/a | yes |
| <a name="input_iam-policy-eks"></a> [iam-policy-eks](#input\_iam-policy-eks) | n/a | `any` | n/a | yes |
| <a name="input_iam-policy-node"></a> [iam-policy-node](#input\_iam-policy-node) | n/a | `any` | n/a | yes |
| <a name="input_iam-role-eks"></a> [iam-role-eks](#input\_iam-role-eks) | n/a | `any` | n/a | yes |
| <a name="input_iam-role-node"></a> [iam-role-node](#input\_iam-role-node) | n/a | `any` | n/a | yes |
| <a name="input_igw-name"></a> [igw-name](#input\_igw-name) | n/a | `any` | n/a | yes |
| <a name="input_rt-name2"></a> [rt-name2](#input\_rt-name2) | n/a | `any` | n/a | yes |
| <a name="input_security-group-name"></a> [security-group-name](#input\_security-group-name) | n/a | `any` | n/a | yes |
| <a name="input_subnet-name"></a> [subnet-name](#input\_subnet-name) | n/a | `any` | n/a | yes |
| <a name="input_subnet-name2"></a> [subnet-name2](#input\_subnet-name2) | n/a | `any` | n/a | yes |
| <a name="input_vpc-name"></a> [vpc-name](#input\_vpc-name) | n/a | `any` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
