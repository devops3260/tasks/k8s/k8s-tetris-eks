variable "aws_region" {
  description = "AWS Region for the S3 and DynamoDB"
}

variable "s3_bucket_name" {
  description = "S3 bucket for holding Terraform state files. Must be globally unique"
}
variable "dynamodb_table_name" {
  description = "DynamoDB table for locking Terraform states"
}
